package edu.utexas.ee382v_julien_project2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.SwingUtilities;

public class Client{
    
    private static Integer BUYER_COUNT = 1;
    private static Integer SELLER_COUNT = 1;
    private SocketManager socketManager;
    private BuyerJFrame buyerFrame;
    private SellerJFrame sellerFrame;
    private Integer id;
    private boolean isBuyer;
    private boolean isClosed;
    private ClientFrame clientFrame;
    
    public Client(boolean isBuyer){
        this.isClosed = false;
        this.isBuyer = isBuyer;
        if (isBuyer) {
            setBuyer();
        } else {
            setSeller();
        }
    }
    
    public boolean isClosed() {
        return isClosed;
    }
    
    public synchronized void setBuyer() {
        this.id = BUYER_COUNT++;
        isBuyer = true;
    }
    
    public synchronized void setSeller() {
        this.id = SELLER_COUNT++;
        isBuyer = false;
    }
    
    public boolean getIsBuyer() {
        return isBuyer;
    }
    
    public Integer getId() {
        return id;
    }
    
    private void connect() throws IOException {
        socketManager = new SocketManager(Broker.ROOT_IP.getSocket(), Broker.ROOT_IP);
        socketManager.write(String.format("%s %s %s", Broker.CLIENT_ROLE, Broker.INIT_ACTION, Broker.ROOT));
    }
    
    private void handleResponse(String response) {
        try {
            String[] args = response.split(" ");
            String action = args[0];
            String content = args[1];
            if (action.equals(Broker.INIT_CLIENT_RESPONSE)) {
                if (content.equals(Broker.NULL)) {
                    System.out.println("No broker servers available at this time. Please try again later");
                    close();
                } else {
                    IpAddress ipAddress = new IpAddress(content);
                    socketManager.write("quit");
                    socketManager.getSocket().close();
                    socketManager = new SocketManager(ipAddress.getSocket(), ipAddress);
                    socketManager.write(String.format("%s %s %s", Broker.CLIENT_ROLE, Broker.INIT_ACTION, Broker.NULL));
                }
            } else if (action.equals(Broker.PUBLISH_ROLE)) {
                handlePublication(args[1], args[2]);
            }
        } catch (Exception e) {
            System.err.println("Error handling response.");
            e.printStackTrace();
        }
    }
    
    public void close() {
        isClosed = true;
        if (buyerFrame != null) {
            buyerFrame.dispose();
        } else if (sellerFrame != null) {
            sellerFrame.dispose();
        }
        if (clientFrame != null) {
            clientFrame.setStatus(Broker.NO_BROKER);
        }
    }
    
    public void handlePublication(String publication, String content) {
        if (publication.equals(Broker.PUB_AVAILABLE_ITEM)) {
            if (buyerFrame != null) {
                buyerFrame.addItem(BidItem.parseBidItem(content));
            }
        } else if (publication.equals(Broker.PUB_BID)) {
            if (sellerFrame != null) {
                sellerFrame.handleBid(content);
            }
        } else if (publication.equals(Broker.PUB_BID_UPDATE)) {
            if (buyerFrame != null) {
                buyerFrame.handleBidUpdate(content);
            }
        } else if (publication.equals(Broker.PUB_SALE_FINALIZED)) {
            if (buyerFrame != null) {
                buyerFrame.handleSaleFinalized(content);
            }
        }
    }
    
    public void attachBuyerFrame(BuyerJFrame buyerFrame) {
        this.buyerFrame = buyerFrame;
        setup();
    }
    
    public void attachSellerFrame(SellerJFrame sellerFrame) {
        this.sellerFrame = sellerFrame;
        setup();
    }
    
    public void attachClientFrame(ClientFrame clientFrame) {
        this.clientFrame = clientFrame;
        clientFrame.setStatus(Broker.WELCOME);
    }
    
    public void setup() {
        try {
            connect();
            launch();
        } catch (Exception e) {
            close();
        }
    }
    
    public void launch() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!isClosed) {
                    if (socketManager.getSocket().isClosed()) {
                        System.out.println("Socket closed!");
                        close();
                        return;
                    }
                    try {
                        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                        socketManager.write(reader.readLine());
                    } catch (IOException e) {
                        System.err.println("Error reading standard input");
                    }
                }
            }
        }).start();
        
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!isClosed) {
                    try {
                        String response = Client.this.socketManager.read();
                        if ((response == null) || response.equals("quit")) {
                            Client.this.socketManager.getSocket().close();
                            close();
                        } else {
                            System.out.println("Response: " + response);
                            handleResponse(response);
                        }
                    } catch (IOException e) {
                        System.err.println("Error reading standard input");
                    }
                }
            }
        }).start();
    }
    
    public void publishItem(BidItem bidItem) {
        String message = String.format("%s %s %s", Broker.PUBLISH_ROLE, Broker.PUB_AVAILABLE_ITEM, bidItem.toString());
        socketManager.write(message);
    }
    
    public void subscribeInterest(String name, String[] attributes, String minBid) {
        String mName = BidItem.encode(name);
        if (mName.trim().isEmpty()) {
            mName = "*";
        }
        
        String mAttributes = "*";
        if (attributes.length > 0) {
            mAttributes = BidItem.getEncodedAttributes(attributes);
        }
        String mMinBid = minBid;
        if (mMinBid.trim().isEmpty()) {
            mMinBid = "*";
        }
        String message = String.format("%s %s %s:%s:%s", Broker.SUBSCRIBE_ROLE, Broker.SUB_INTEREST, mName, mAttributes, mMinBid);
        socketManager.write(message);
    }
    
    public void subscribeReceiveBid(Integer itemId) {
        String message = String.format("%s %s %s", Broker.SUBSCRIBE_ROLE, Broker.SUB_RECEIVE_BID, itemId.toString());
        socketManager.write(message);
    }
    
    public void subscribeItemSold(Integer itemId) {
        String message = String.format("%s %s %s", Broker.SUBSCRIBE_ROLE, Broker.SUB_ITEM_SOLD, itemId.toString());
        socketManager.write(message);
    }
    
    public void subscribeInterestBidUpdate(Integer itemId) {
        String message = String.format("%s %s %s", Broker.SUBSCRIBE_ROLE, Broker.SUB_INTEREST_BID_UPDATE, itemId.toString());
        socketManager.write(message);
    }
    
    public void publishBid(Integer itemId, Double bid) {
        String message = String.format("%s %s %s:%s:%s", Broker.PUBLISH_ROLE, Broker.PUB_BID, id, itemId.toString(), bid.toString());
        socketManager.write(message);
    }
    
    public void publishBidUpdate(Integer buyerId, Integer itemId, Double bid) {
        String message = String.format("%s %s %s:%s:%s", Broker.PUBLISH_ROLE, Broker.PUB_BID_UPDATE, buyerId.toString(), itemId.toString(), bid.toString());
        socketManager.write(message);
    }
    
    public void closeSale(BidItem bidItem) {
        String message = String.format("%s %s %s:%s:%s", Broker.PUBLISH_ROLE, Broker.PUB_SALE_FINALIZED, bidItem.getById(), bidItem.getId().toString(), bidItem.getCurrentBid());
        socketManager.write(message);
    }

    

}

