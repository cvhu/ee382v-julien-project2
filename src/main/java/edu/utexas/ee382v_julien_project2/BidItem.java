package edu.utexas.ee382v_julien_project2;

import java.util.Arrays;

public class BidItem {
    public final static String NA = "n/a";
    private final static String SPACE = "%S%";
    private final static String COMMA = "%C%";
    private final static String COLON = "%CC%";
    private static Integer COUNT = 1;
    private Integer id;
    private String name;
    private String[] attributes;
    private Double minBid;
    private Double currentBid;
    private Integer by;
    private boolean isFinal;
    
    public static void main(String[] args) {
        BidItem.matchInterest("1:woody:toy,story:12.0", "*:*:*");
    }
    
    public BidItem(String name, String[] attributes, Double minBid) {
        this(COUNT, name, attributes, minBid);
        COUNT++;
    }
    
    public BidItem(Integer id, String name, String[] attributes, Double minBid) {
        this.id = id;
        this.name = name;
        this.attributes = attributes;
        this.minBid = minBid;
        this.currentBid = -1.0;
        this.by = -1;
        this.isFinal = false;
    }
    
    public static BidItem parseBidItem(String content) {
        try {
            String[] args = content.split(":");
            if (args.length == 3) {
                return new BidItem(args[0], getDecodedAttributes(args[1]), Double.parseDouble(args[2]));
            } else if (args.length == 4) {
                return new BidItem(Integer.parseInt(args[0]), args[1], getDecodedAttributes(args[2]), Double.parseDouble(args[3]));
            }
        } catch (Exception e) {
            System.err.println("Error parsing biditem: " + content);
            e.printStackTrace();
        }
        return null;
    }
    
    public static boolean matchInterest(String itemContent, String interestContent) {
        boolean match = false;
        try {
            String[] itemArgs = itemContent.split(":");
            System.out.println(itemContent + Arrays.asList(itemArgs).toString() + itemArgs.length);
            String itemName = itemArgs[1];
            String[] itemAttributes = itemArgs[2].split(",");
            Double itemMinBid = Double.parseDouble(itemArgs[3]);
            
            String[] interestArgs = interestContent.split(":");
            System.out.println(Arrays.asList(interestArgs).toString());
            String interestName = interestArgs[0].trim();
            
            boolean nameMatch = false;
            if (interestName != null) {
                nameMatch = interestName.equals(itemName);
                if (interestName.isEmpty() || interestName.equals("*")) {
                    nameMatch = true;
                }
            }
            
            boolean attributesMatch = false;
            if (interestArgs[1] != null) {
                if (interestArgs[1].trim().equals("*")) {
                    attributesMatch = true;
                } else {
                    String[] interestAttributes = interestArgs[1].trim().split(",");
                    System.out.println(Arrays.asList(interestAttributes).toString());
                    attributesMatch = true;
                    for (int ind = 0; ind < interestAttributes.length; ind++) {
                        String interestAttribute = interestAttributes[ind].trim();
                        System.out.println(interestAttribute);
                        if (!interestAttribute.isEmpty() && !Arrays.asList(itemAttributes).contains(interestAttribute)) {
                            attributesMatch = false;
                        }
                    }
                }
            }
            
            boolean minBidMatch = false;
            if (interestArgs[2] != null) {
                if (interestArgs[2].trim().isEmpty() || interestArgs[2].equals("*")) {
                    minBidMatch = true;
                } else {
                    Double interestMinBid = Double.parseDouble(interestArgs[2]);
                    if (interestMinBid >= itemMinBid) {
                        minBidMatch = true;
                    }
                }
            }
            match = (nameMatch && attributesMatch && minBidMatch);
            System.out.printf("%s = %s & %s & %s\n", match, nameMatch, attributesMatch, minBidMatch);
        } catch (Exception e) {
            System.err.println("!!Exception in matching interest");
            e.printStackTrace();
        }
        return match;
    }
    
    public Integer getId() {
        if (id == null) {
            return 0;
        }
        return id;
    }
    
    public void setCurrentBid(Double bid) {
        this.currentBid = bid;
    }
    
    public void setById(Integer buyerId) {
        this.by = buyerId;
    }
    
    public void setIsFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }
    
    public String getName() {
        return name;
    }
    
    public String[] getAttribute() {
        return attributes;
    }
    
    public Double getMinBid() {
        if (minBid == null) {
            return 0.0;
        }
        return minBid;
    }
    
    public String getCurrentBid() {
        if (currentBid < 0.0) {
            return NA;
        }
        return currentBid.toString();
    }
    
    public String getById() {
        if (by < 0.0) {
            return NA;
        }
        return by.toString();
    }
    
    public boolean getIsFinal() {
        return isFinal;
    }
    
    public boolean takeBid(Integer buyerId, Double bid) {
        boolean updated = false;
        if ((bid >= minBid) && (bid > currentBid)) {
            currentBid = bid;
            by = buyerId;
            updated = true;
        }
        return updated;
    }
    
    public String toString() {
        return String.format("%d:%s:%s:%s", id, encode(name), getEncodedAttributes(attributes), minBid.toString());
    }
    
    public String getAttributes() {
        return Arrays.toString(attributes);
    }
    
    public static String getEncodedAttributes(String[] attributes) {
        StringBuffer buffer = new StringBuffer();
        int n = attributes.length;
        for (int ind = 0; ind < (n - 1); ind++) {
            buffer.append(encode(attributes[ind]));
            buffer.append(",");
        }
        buffer.append(attributes[n - 1]);
        return buffer.toString();
    }
    
    public static String[] getDecodedAttributes(String decode) {
        String[] decoded = decode.split(",");
        for (int ind = 0; ind < decoded.length; ind++) {
            decoded[ind] = decode(decoded[ind]);
        }
        return decoded;
    }
    
    public String[] getData() {
        String[] data = {id.toString(), name, getAttributes(), minBid.toString()};
        return data;
    }
    
    public static String encode(String toEncode) {
        String encoded = toEncode.replaceAll(" ", SPACE);
        encoded = encoded.replaceAll(",", COMMA);
        encoded = encoded.replaceAll(":", COLON);
        return encoded;
    }
    
    public static String decode(String toDecode) {
        String decoded = toDecode.replaceAll(SPACE, " ");
        decoded = decoded.replaceAll(COMMA, ",");
        decoded = decoded.replaceAll(COLON, ":");
        return decoded;
    }
}
