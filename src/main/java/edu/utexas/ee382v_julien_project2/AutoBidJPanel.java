package edu.utexas.ee382v_julien_project2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

public class AutoBidJPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private JTextField txtMax;
    private JTextField txtInc;
    private ItemJPanel itemPanel;
    private JButton btnStart;

    /**
     * Create the panel.
     */
    public AutoBidJPanel(ItemJPanel itemPanel) {
        this.itemPanel = itemPanel;
        
        txtMax = new JTextField();
        txtMax.setText("Max");
        txtMax.setColumns(10);
        
        txtInc = new JTextField();
        txtInc.setText("Inc");
        txtInc.setColumns(10);
        
        btnStart = new JButton("Start");
        btnStart.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                AutoBidJPanel.this.itemPanel.startAutoBid(txtMax.getText(), txtInc.getText());
                btnStart.setEnabled(false);
                txtInc.setEnabled(false);
                txtMax.setEnabled(false);
            }
        });
        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.TRAILING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtMax, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(txtInc, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(btnStart, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                    .addGap(1))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
                    .addContainerGap(16, Short.MAX_VALUE)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(txtMax, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtInc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnStart))
                    .addContainerGap())
        );
        setLayout(groupLayout);

    }

}
