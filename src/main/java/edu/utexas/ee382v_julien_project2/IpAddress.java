/*
 * EE382V Julien Project 2
 * 
 * CH26773 Chinwei Hu
 */
package edu.utexas.ee382v_julien_project2;

import static java.lang.System.err;

import java.net.ServerSocket;
import java.net.Socket;

public class IpAddress {
    private String address;
    private int port;

    public IpAddress(String ip) {
        this(ip.split(":")[0], Integer.parseInt(ip.split(":")[1]));
    }

    public IpAddress(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public Socket getSocket() {
        try {
            return new Socket(address, port);
        } catch (Exception e) {
            err.println(e);
            return null;
        }
    }
    
    public ServerSocket getServerSocket() {
        try {
            return new ServerSocket(port);
        } catch (Exception e) {
            err.println(e);
            return null;
        }
    }
    
    public boolean isLocalhost(){
        return address.equals("localhost") || address.equals("0.0.0.0") || address.equals("127.0.0.1");
    }
    
    public String toString() {
        return String.format("%s:%d", address, port);
    }
}
