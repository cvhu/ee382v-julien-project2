package edu.utexas.ee382v_julien_project2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ClientFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private JLabel lblStatus;
    
    public ClientFrame() {
        init();
    }
    
    public void init() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Client");
        setLayout(new FlowLayout());
        setSize(500, 150);
        setLocationRelativeTo(null);
        lblStatus = new JLabel(Broker.WELCOME);
        add(lblStatus);
        
        JPanel panel = new JPanel();
        
        JButton buyerButton = new JButton("Join as Buyer");
        buyerButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                System.out.println("Buyer selected");
                final Client client = new Client(true);
                client.attachClientFrame(ClientFrame.this);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        BuyerJFrame buyerFrame = new BuyerJFrame(client);
                        buyerFrame.setVisible(true);
                    }
                });
            }
        });
        panel.add(buyerButton);
        
        JButton sellerButton = new JButton("Join as Seller");
        sellerButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Seller selected");
                final Client client = new Client(false);
                client.attachClientFrame(ClientFrame.this);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        SellerJFrame sellerFrame = new SellerJFrame(client);
                        sellerFrame.setVisible(true);
                    }
                });
            }
        });
        panel.add(sellerButton);
        
        add(panel);
    }
    
    public void setStatus(String status) {
        lblStatus.setText(status);
    }
    
    public static void main(String[] args) throws Exception{
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ClientFrame clientFrame = new ClientFrame();
                clientFrame.setVisible(true);
            }
        });
    }
}