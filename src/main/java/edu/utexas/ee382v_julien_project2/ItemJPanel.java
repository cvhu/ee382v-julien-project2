package edu.utexas.ee382v_julien_project2;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class ItemJPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    public final static String CHOOSE = "Choose";
    public final static String USERDRIVEN = "User-Driven";
    public final static String AUTO = "Auto";
    public final static String FINALIZED = "Finalized";
    private BidItem bidItem;
    private Client client;
    private boolean bidded;
    private Double maxBid;
    private Double incBid;
    private JLabel lblItemname;
    private JLabel lblItemid;
    private JLabel lblItemattributes;
    private JLabel lblItemminbid;
    private JLabel lblItemcurrentbid;
    private JLabel lblMycurrentbid;
    private JLabel lblByUser;
    private JLabel lblFinal;
    private boolean isAuto;
    private CardLayout cl;
    private JPanel cards;
    private AutoBidJPanel autoPanel;
    private ChooseModeJPanel choosePanel;
    private UserDrivenBidJPanel userPanel;
    private FinalizedBidJPanel finalizedPanel;

    /**
     * Create the panel.
     */
    public ItemJPanel(BidItem bidItem, Client client) {
        this.isAuto = false;
        this.bidItem = bidItem;
        this.client = client;
        this.bidded = false;
        
        System.out.println("new item panel");
        
        lblItemname = new JLabel(bidItem.getName());
        lblItemname.setFont(new Font("Lucida Grande", Font.BOLD, 13));
        
        lblItemid = new JLabel(bidItem.getId().toString());
        lblItemid.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
        
        lblItemattributes = new JLabel(bidItem.getAttributes());
        lblItemattributes.setFont(new Font("Lucida Grande", Font.ITALIC, 10));
        
        lblItemminbid = new JLabel("min:" + bidItem.getMinBid().toString());
        
        lblItemcurrentbid = new JLabel("current:");
        
        lblMycurrentbid = new JLabel("mine:");

        JPanel panelControl = new JPanel();
        
        
        panelControl.setBackground(SystemColor.window);
        
        lblByUser = new JLabel("By User:");
        
        lblFinal = new JLabel("Final?");
        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(lblItemid)
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addComponent(lblItemname))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(lblItemcurrentbid)
                            .addGap(18)
                            .addComponent(lblByUser))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(lblItemminbid)
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addComponent(lblMycurrentbid)))
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addComponent(lblFinal)
                        .addComponent(lblItemattributes))
                    .addPreferredGap(ComponentPlacement.UNRELATED)
                    .addComponent(panelControl, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.TRAILING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(lblItemname)
                                .addComponent(lblItemid)
                                .addComponent(lblItemattributes))
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(lblItemminbid)
                                .addComponent(lblMycurrentbid))
                            .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(lblItemcurrentbid)
                                .addComponent(lblByUser)
                                .addComponent(lblFinal)))
                        .addComponent(panelControl, GroupLayout.PREFERRED_SIZE, 66, Short.MAX_VALUE))
                    .addContainerGap())
        );
        setLayout(groupLayout);
        
        cl = new CardLayout();
        cards = new JPanel(cl);
        
        autoPanel = new AutoBidJPanel(this);
        cards.add(autoPanel, AUTO);
        choosePanel = new ChooseModeJPanel(cards);
        cards.add(choosePanel, CHOOSE);
        userPanel = new UserDrivenBidJPanel(this);
        cards.add(userPanel, USERDRIVEN);
        finalizedPanel = new FinalizedBidJPanel();
        cards.add(finalizedPanel, FINALIZED);
        cl.show(cards, CHOOSE);
        panelControl.setLayout(new BorderLayout(0, 0));
        panelControl.add(cards, BorderLayout.CENTER);
        
        setBidItem(bidItem);
    }
    
    public void setBidItem(BidItem bidItem) {
        this.bidItem = bidItem;
        lblItemid.setText("ID:" + bidItem.getId());
        lblItemname.setText("Name:" + bidItem.getName());
        lblItemattributes.setText("Attributes:" + bidItem.getAttributes());
        lblItemminbid.setText("Min:" + bidItem.getMinBid());
        lblItemcurrentbid.setText("Current:" + bidItem.getCurrentBid());
        lblFinal.setText("Final? " + bidItem.getIsFinal());
        if (bidItem.getIsFinal()) {
            finalizedPanel.setLabel("Finalized");
            cl.show(cards, FINALIZED);
        }
        if (bidItem.getById().equals(client.getId().toString())) {
            lblByUser.setText("By Me(" + bidItem.getById() + ")");
            finalizedPanel.setLabel("You Win!");
        } else {
            lblByUser.setText("By User:" + bidItem.getById());
        }
    }
    
    public void startAutoBid(String max, String inc) {
        try {
            this.isAuto = true;
            this.maxBid = Double.parseDouble(max);
            this.incBid = Double.parseDouble(inc);
            autoBid();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void autoBid() {
        if (isAuto && !bidItem.getById().equals(client.getId().toString())) {
            String currentBidS = bidItem.getCurrentBid();
            Double bid = bidItem.getMinBid() + incBid;
            if (!currentBidS.equals(BidItem.NA)) {
                try {
                    bid = Double.parseDouble(bidItem.getCurrentBid()) + incBid;
                } catch (Exception e) {
                    System.err.println("Error parsing current bid in auto mode");
                    e.printStackTrace();
                }
            }
            if (bid <= maxBid) {
                sendBid(bid.toString());
            }
        }
    }
    
    public Integer getItemId() {
        return bidItem.getId();
    }
    
    public BidItem getBidItem() {
        return bidItem;
    }
    
    public void sendBid(String bid) {
        try {
            Double bidD = Double.parseDouble(bid);
            client.publishBid(bidItem.getId(), bidD);
            if (!bidded) {
                bidded = true;
                client.subscribeInterestBidUpdate(bidItem.getId());
                client.subscribeItemSold(bidItem.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
