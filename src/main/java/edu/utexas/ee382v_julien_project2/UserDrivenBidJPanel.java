package edu.utexas.ee382v_julien_project2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

public class UserDrivenBidJPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private JTextField txtMybid;
    private ItemJPanel itemPanel;

    /**
     * Create the panel.
     */
    public UserDrivenBidJPanel(ItemJPanel itemPanel) {
        this.itemPanel = itemPanel;
        
        txtMybid = new JTextField();
        txtMybid.setText("myBid");
        txtMybid.setColumns(10);
        
        JButton btnSendBid = new JButton("Send Bid");
        btnSendBid.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                UserDrivenBidJPanel.this.itemPanel.sendBid(txtMybid.getText());
            }
        });
        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(txtMybid, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(btnSendBid)
                    .addContainerGap(11, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
                    .addContainerGap(18, Short.MAX_VALUE)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(txtMybid, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSendBid))
                    .addContainerGap())
        );
        setLayout(groupLayout);

    }
}
