/*
 * EE382V Julien Project 2
 * 
 * CH26773 Chinwei Hu
 */
package edu.utexas.ee382v_julien_project2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketManager {
    private Socket socket;
    private IpAddress ipAddress;
    private BufferedReader reader;
    private PrintWriter writer;
    
    public SocketManager(Socket socket, IpAddress ipAddress) {
        this.socket = socket;
        this.ipAddress = ipAddress;
        try {
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.writer = new PrintWriter(socket.getOutputStream());
        } catch (Exception e) {
            System.err.println("Error creating Socket Manager");
            //e.printStackTrace();
        }
        
    }
    
    public Socket getSocket() {
        return socket;
    }
    
    public void setIpAddress(IpAddress ipAddress) {
        this.ipAddress = ipAddress;
    }
    
    public String toString() {
        return ipAddress.toString();
    }
    
    public void write(String content) {
        if (socket.isClosed()) {
            System.out.println("Cannot write: socket closed.");
        } else {
            System.out.printf("-writing<%s>:%s\n", toString(), content);
            writer.println(content);
            writer.flush();
        }
    }
    
    public String read() {
        if (socket.isClosed()) {
            System.out.println("Cannot read: socket closed.");
            return null;
        } else {
            try {
                String line = reader.readLine();
                System.out.printf("-reading<%s>:%s\n", toString(), line);
                return line;
            } catch (Exception e) {
                System.err.println("Error reading socket");
                //e.printStackTrace();
                return null;
            }
        }
    }
    
    public void close() throws IOException {
        socket.close();
    }
}
