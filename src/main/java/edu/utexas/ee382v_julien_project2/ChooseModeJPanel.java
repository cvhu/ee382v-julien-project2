package edu.utexas.ee382v_julien_project2;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class ChooseModeJPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    /**
     * Create the panel.
     */
    public ChooseModeJPanel(final JPanel cards) {
        setBorder(new TitledBorder("Start Bidding"));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{38, 103, 0};
        gridBagLayout.rowHeights = new int[]{29, 0, 29, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);
        
        JButton btnAuto = new JButton("auto");
        btnAuto.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, ItemJPanel.AUTO);
            }
        });
        
        JButton btnUserdriven = new JButton("User-Driven");
        btnUserdriven.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, ItemJPanel.USERDRIVEN);
            }
        });
        btnUserdriven.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
        GridBagConstraints gbc_btnUserdriven = new GridBagConstraints();
        gbc_btnUserdriven.anchor = GridBagConstraints.NORTHWEST;
        gbc_btnUserdriven.insets = new Insets(0, 0, 5, 5);
        gbc_btnUserdriven.gridx = 0;
        gbc_btnUserdriven.gridy = 1;
        add(btnUserdriven, gbc_btnUserdriven);
        btnAuto.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
        GridBagConstraints gbc_btnAuto = new GridBagConstraints();
        gbc_btnAuto.anchor = GridBagConstraints.NORTH;
        gbc_btnAuto.gridx = 1;
        gbc_btnAuto.gridy = 1;
        add(btnAuto, gbc_btnAuto);
    }
}
