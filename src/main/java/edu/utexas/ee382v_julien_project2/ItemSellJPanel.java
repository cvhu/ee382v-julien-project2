package edu.utexas.ee382v_julien_project2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;


public class ItemSellJPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private JLabel lblId;
    private JLabel lblName;
    private JLabel lblAttributes;
    private JLabel lblMin;
    private JLabel lblCurrent;
    private JLabel lblBy;
    private JLabel lblFinal;
    private BidItem bidItem;
    private Client client;
    private JButton btnCloseSale;

    /**
     * Create the panel.
     */
    public ItemSellJPanel(BidItem bidItem, Client client) {
        this.client = client;
        this.bidItem = bidItem;
        
        lblId = new JLabel("ID:" + bidItem.getId());
        
        lblName = new JLabel("Name:");
        
        lblAttributes = new JLabel("Attributes:");
        
        lblMin = new JLabel("Min:");
        
        lblCurrent = new JLabel("Current:");
        
        lblBy = new JLabel("By:");
        
        lblFinal = new JLabel("Final?");
        
        btnCloseSale = new JButton("Close Sale");
        btnCloseSale.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                BidItem bidItem = ItemSellJPanel.this.bidItem;
                bidItem.setIsFinal(true);
                ItemSellJPanel.this.setBidItem(bidItem);
                ItemSellJPanel.this.client.closeSale(bidItem);
                btnCloseSale.setEnabled(false);
            }
        });
        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                .addComponent(lblCurrent)
                                .addComponent(lblId, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE))
                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                .addGroup(groupLayout.createSequentialGroup()
                                    .addPreferredGap(ComponentPlacement.RELATED)
                                    .addComponent(lblName))
                                .addGroup(groupLayout.createSequentialGroup()
                                    .addGap(20)
                                    .addComponent(lblBy))))
                        .addComponent(lblMin))
                    .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addComponent(lblAttributes, Alignment.TRAILING)
                        .addComponent(btnCloseSale, Alignment.TRAILING)
                        .addComponent(lblFinal, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
                    .addGap(53))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addGap(5)
                            .addComponent(lblAttributes))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(lblId)
                                .addComponent(lblName))))
                    .addGap(18)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(lblMin)
                        .addComponent(lblFinal))
                    .addGap(3)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnCloseSale)
                        .addComponent(lblCurrent)
                        .addComponent(lblBy))
                    .addContainerGap())
        );
        setLayout(groupLayout);
        setBidItem(bidItem);
    }
    
    public BidItem getBidItem() {
        return bidItem;
    }
    
    public void setBidItem(BidItem bidItem) {
        this.bidItem = bidItem;
        lblId.setText("ID:" + bidItem.getId());
        lblName.setText("Name:" + bidItem.getName());
        lblAttributes.setText("Attributes:" + bidItem.getAttributes());
        lblMin.setText("Min:" + bidItem.getMinBid());
        lblCurrent.setText("Current:" + bidItem.getCurrentBid());
        lblBy.setText("By Buyer:" + bidItem.getById());
        lblFinal.setText("Final? " + bidItem.getIsFinal());
    }

}
