package edu.utexas.ee382v_julien_project2;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FinalizedBidJPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private JLabel lblFinalized;
    /**
     * Create the panel.
     */
    public FinalizedBidJPanel() {
        
        lblFinalized = new JLabel("Finalized");
        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGap(49)
                    .addComponent(lblFinalized)
                    .addContainerGap(56, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.TRAILING)
                .addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
                    .addGap(17)
                    .addComponent(lblFinalized)
                    .addContainerGap(18, Short.MAX_VALUE))
        );
        setLayout(groupLayout);

    }
    
    public void setLabel(String content) {
        lblFinalized.setText(content);
    }
}
