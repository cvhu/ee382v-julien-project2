/*
 * EE382V Julien Project 2
 * 
 * CH26773 Chinwei Hu
 */
package edu.utexas.ee382v_julien_project2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Broker {
    
    // Every broker should know where the root is.
    public static final IpAddress ROOT_IP = new IpAddress("127.0.0.1", 3000);
    
    // String Constants
    public static final String BROKER_ROLE = "broker";
    public static final String CLIENT_ROLE = "client";
    public static final String PUBLISH_ROLE = "publish";
    public static final String SUBSCRIBE_ROLE = "subscribe";
    
    public static final String PUB_AVAILABLE_ITEM = "available_item";
    public static final String PUB_BID = "bid";
    public static final String PUB_BID_UPDATE = "bid_update";
    public static final String PUB_SALE_FINALIZED = "sale_finalized";
    public static final String SUB_INTEREST = "interest";
    public static final String SUB_RECEIVE_BID = "receive_bid";
    public static final String SUB_INTEREST_BID_UPDATE = "interest_bid_update";
    public static final String SUB_ITEM_SOLD = "item_sold";
    
    public static final String INIT_ACTION = "init";
    public static final String INIT_RESPONSE = "init_response";
    public static final String INIT_CLIENT_RESPONSE = "init_client_response";
    public static final String UPDATE_FRONTBROKER_ACTION = "update_frontbroker";
    public static final String UPDATE_FRONTBROKER_RESPONSE = "update_frontbroker_response";
    public static final String BROADCAST_ACTION = "broadcast";
    public static final String ROOT = "root";
    public static final String NULL = "NULL";
    
    public static final String NO_BROKER = "No broker is current available. Please add more brokers and try again.";
    public static final String WELCOME = "Welcome to the Bidding Network!";
    
    // The broker's ip address.
    private IpAddress myIpAddress;
    
    // Whether the broker is root.
    private boolean isRoot;
    
    // Listener server socket.
    private ServerSocket listener;
    
    // Housekeeping variables for the broker hierarchy.
    private SocketManager parentSM;
    private SocketManager leftSM;
    private SocketManager rightSM;
    private SocketManager clientSM;
    private List<IpAddress> availableIp;
    private Integer leftBrokerCount;
    private Integer rightBrokerCount;
    private HashMap<String, List<String>> leftSubs;
    private HashMap<String, List<String>> rightSubs;
    private HashMap<String, List<String>> clientSubs;
    
    public Broker(String address) {
        leftSubs = new HashMap<String, List<String>>();
        rightSubs = new HashMap<String, List<String>>();
        clientSubs = new HashMap<String, List<String>>();
        if (address.equals("root")) {
            myIpAddress = ROOT_IP;
            isRoot = true;
            availableIp = new ArrayList<IpAddress>();
            availableIp.add(ROOT_IP);
        } else {
            try {
                myIpAddress = new IpAddress(address);
                isRoot = false;
            } catch (Exception e) {
                System.err.println("Invalid IP address");
                e.printStackTrace();
            }
        }
        
        if ((myIpAddress != null) && myIpAddress.isLocalhost()) {
            leftBrokerCount = 0;
            rightBrokerCount = 0;
            launch();
        } else {
            System.out.println("invalid ip address:" + address);
        }
    }
    
    public void subscribe(String subscription, String content, SocketManager socketManager) {
        if ((leftSM != null) && leftSM.equals(socketManager)) {
            addSubscription(leftSubs, subscription, content);
        } else if ((rightSM != null) && rightSM.equals(socketManager)) {
            addSubscription(rightSubs, subscription, content);
        } else if ((clientSM != null) && clientSM.equals(socketManager)) {
            addSubscription(clientSubs, subscription, content);
        }
        if (parentSM != null) {
            String message = String.format("%s %s %s", SUBSCRIBE_ROLE, subscription, content);
            parentSM.write(message);
        }
    }
    
    public void addSubscription(HashMap<String, List<String>> subsMap, String subscription, String content) {
        List<String> subs = subsMap.get(subscription);
        if (subs == null) {
            subs = new ArrayList<String>();
        }
        subs.add(content);
        subsMap.put(subscription, subs);
        System.out.printf("Added subscription[%s]:%s\n", subscription, subs.toString());
    }
    
    public void publish(String publication, String content, SocketManager socketManager) {
        String message = String.format("%s %s %s", PUBLISH_ROLE, publication, content);
        if ((leftSM != null) && leftSM.equals(socketManager)) {
            sendPublication(publication, content, clientSM, clientSubs);
            sendPublication(publication, content, rightSM, rightSubs);
            if (parentSM != null) {
                parentSM.write(message);
            }
        } else if ((rightSM != null) && rightSM.equals(socketManager)) {
            sendPublication(publication, content, leftSM, leftSubs);
            sendPublication(publication, content, clientSM, clientSubs);
            if (parentSM != null) {
                parentSM.write(message);
            }
        } else if ((clientSM != null) && clientSM.equals(socketManager)) {
            sendPublication(publication, content, rightSM, rightSubs);
            sendPublication(publication, content, leftSM, leftSubs);
            if (parentSM != null) {
                parentSM.write(message);
            }
        } else if ((parentSM != null) && parentSM.equals(socketManager)) {
            sendPublication(publication, content, rightSM, rightSubs);
            sendPublication(publication, content, leftSM, leftSubs);
            sendPublication(publication, content, clientSM, clientSubs);
        }
    }
    
    public void sendPublication(String publication, String content, SocketManager socketManager, HashMap<String, List<String>> subsMap) {
        if (socketManager != null) {
            List<String> subs;
            boolean match = false;
            if (publication.equals(PUB_AVAILABLE_ITEM)) {
                subs = subsMap.get(SUB_INTEREST);
                if ((subs != null) && !subs.isEmpty()) {
                    for (String interest : subs) {
                        if (BidItem.matchInterest(content, interest)) {
                            match = true;
                            break;
                        }
                    }
                }
            } else if (publication.equals(PUB_BID)) {
                subs = subsMap.get(SUB_RECEIVE_BID);
                if ((subs != null) && !subs.isEmpty()) {
                    for (String itemId : subs) {
                        String[] args = content.split(":");
                        if (args[1].equals(itemId)) {
                            match = true;
                            break;
                        }
                    }
                }
            } else if (publication.equals(PUB_BID_UPDATE)) {
                subs = subsMap.get(SUB_INTEREST_BID_UPDATE);
                if ((subs != null) && !subs.isEmpty()) {
                    for (String itemId : subs) {
                        String[] args = content.split(":");
                        if (args[1].equals(itemId)) {
                            match = true;
                            break;
                        }
                    }
                }
            } else if (publication.equals(PUB_SALE_FINALIZED)) {
                subs = subsMap.get(SUB_ITEM_SOLD);
                if ((subs != null) && !subs.isEmpty()) {
                    for (String itemId : subs) {
                        String[] args = content.split(":");
                        if (args[1].equals(itemId)) {
                            match = true;
                            break;
                        }
                    }
                }
            }
            if (match) {
                String message = String.format("%s %s %s", PUBLISH_ROLE, publication, content);
                socketManager.write(message);
            }
        }
    }
    
    public IpAddress getIpAddress() {
        return myIpAddress;
    }
    
    public void launch() {
        if (!isRoot) {
            connectToRoot();
        }
        startServer();
    }
    
    public void startServer() {
        listener = myIpAddress.getServerSocket();
        System.out.println("Broker server started...");
        try {
            while(true) {
                new Handler(listener.accept(), this).start();
            }
        } catch (Exception e) {
            System.err.println("Error accepting socket.");
            e.printStackTrace();
        } finally {
            try {
                listener.close();
            } catch (IOException e) {
                System.err.println("Error closing listener");
                e.printStackTrace();
            }
        }
    }
    
    public void setParentSM(SocketManager socketManager) {
        parentSM = socketManager;
    }
    
    public void addBroker(String ipString) {
        try{
            String message;
            IpAddress ipAddress = new IpAddress(ipString);
            if (isRoot && (availableIp != null)) {
                availableIp.add(ipAddress);
            }
            if ((leftSM == null) || (rightSM == null)) {
                message = String.format("%s %s %s", BROKER_ROLE, INIT_RESPONSE, myIpAddress.toString());
                Thread.sleep(1000);
                SocketManager socketManager = initSocket(ipAddress, message);
                socketManager.setIpAddress(ipAddress);
                if (leftSM == null) {
                    leftSM = socketManager;
                    leftBrokerCount = 1;
                } else {
                    rightSM = socketManager;
                    rightBrokerCount = 1;
                }
            } else {
                message = String.format("%s %s %s", BROKER_ROLE, INIT_ACTION, ipString);
                if (leftBrokerCount <= rightBrokerCount) {
                    leftBrokerCount++;
                    leftSM.write(message);
                } else {
                    rightBrokerCount++;
                    rightSM.write(message);
                }
            }
            printBrokerStats();
        } catch (Exception e) {
            System.err.println("Error adding broker ");
            e.printStackTrace();
        }
    }
    
    public void addClient(String content, SocketManager socketManager) {
        if (content.equals(ROOT)) {
            if (isRoot) {
                if (!availableIp.isEmpty()) {
                    IpAddress pendingBrokerIp = availableIp.remove(0);
                    socketManager.write(String.format("%s %s", INIT_CLIENT_RESPONSE, pendingBrokerIp.toString()));
                } else {
                    socketManager.write(String.format("%s %s", INIT_CLIENT_RESPONSE, NULL));
                    try {
                        socketManager.getSocket().close();
                    } catch (Exception e) {
                        System.err.println("unavailable server causes socket closure.");
                    }
                }
            }
        } else if (content.equals(NULL)) {
            if ((clientSM == null) || clientSM.getSocket().isClosed()) {
                clientSM = socketManager;
            }
        }
        printBrokerStats();
    }
    
    public void addParentBroker(SocketManager socketManager, String content) {
        if (!isRoot) {
            parentSM = socketManager;
            socketManager.write("ack added parent broker");
            socketManager.setIpAddress(new IpAddress(content));
        }
    }
    
    public void printBrokerStats() {
        System.out.printf("Left: %d %s Right: %d %s Parent: %s Client: %s\n", leftBrokerCount, leftSM, rightBrokerCount, rightSM, parentSM, clientSM);
    }
    
    public void connectToRoot() {
        System.out.println("connect to root..");
        initSocket(ROOT_IP, String.format("%s %s %s", BROKER_ROLE, INIT_ACTION, myIpAddress.toString()));
    }
    
    public SocketManager initSocket(IpAddress toIp, String content) {
        try {
            Handler handler = new Handler(toIp.getSocket(), this);
            SocketManager socketManager = handler.getSocketManager();
            socketManager.write(content);
            handler.start();
            return socketManager;
        } catch (Exception e) {
            System.err.println("Error initiating socket: '" + content + "' to " + toIp.toString());
            e.printStackTrace();
        }
        return null;
    }
    
    public String buildMessage(String role, String action, String content) {
        return String.format("%s %s %s", role, action, content);
    }
    
    public void brokerBroadcast(String content, SocketManager fromSM) {
        if ((clientSM != null) && !clientSM.equals(fromSM)) {
            clientSM.write(buildMessage(BROKER_ROLE, BROADCAST_ACTION, content));
        }
        clientBroadcast(content, fromSM);
    }
    
    public void clientBroadcast(String content, SocketManager fromSM) {
        String message = buildMessage(BROKER_ROLE, BROADCAST_ACTION, content);
        if ((parentSM != null) && !parentSM.equals(fromSM)) {
            parentSM.write(message);
        }
        if ((leftSM != null) && !leftSM.equals(fromSM)) {
            leftSM.write(message);
        }
        if ((rightSM != null) && !rightSM.equals(fromSM)) {
            rightSM.write(message);
        }
    }
    
    public static void main(String[] args) {
        if (args.length == 1) {
            new Broker(args[0]);
        } else {
            System.err.println("Invalid inputs: Broker <IP Address>|root ");
        }

    }
    
    public class Handler extends Thread {
        private Broker broker;
        private Socket socket;
        private SocketManager socketManager;
        
        public Handler(Socket socket, Broker broker) {
            this.socket = socket;
            this.broker = broker;
            this.socketManager = new SocketManager(socket, broker.getIpAddress());
        }
        
        public SocketManager getSocketManager() {
            return socketManager;
        }
        
        public void handleRequest(String request) {
            System.out.println("Handling request: " + request);
            try {
                String[] args = request.split(" ");
                String role = args[0];
                String action = args[1];
                String content = args[2];
                if (role.equals(BROKER_ROLE)) {
                    handleBrokerRequest(action, content);
                } else if (role.equals(CLIENT_ROLE)) {
                    handleClientRequest(action, content);
                } else if (role.equals(SUBSCRIBE_ROLE)) {
                    broker.subscribe(action, content, socketManager);
                } else if (role.equals(PUBLISH_ROLE)) {
                    broker.publish(action, content, socketManager);
                }
            } catch (Exception e) {
                socketManager.write("Invalid request: " + request);
                System.err.println("Invalid request: " + request);
                e.printStackTrace();
            }
        }
        
        public void handleBrokerRequest(String action, String content) {
            if (action.equals(INIT_ACTION)) {
                if (broker.isRoot) {
                    socketManager.write("quit");
                }
                broker.addBroker(content);
            } else if (action.equals(INIT_RESPONSE)) {
                broker.addParentBroker(socketManager, content);
            } else if (action.equals(BROADCAST_ACTION)) {
                brokerBroadcast(content, socketManager);
            }
        }

        public void handleClientRequest(String action, String content) {
            if (action.equals(INIT_ACTION)) {
                broker.addClient(content, socketManager);
            } else if (action.equals(BROADCAST_ACTION)) {
                clientBroadcast(content, socketManager);
            }
        }
        
        public void run() {
            try {
                while (true) {
                    if (socketManager.getSocket().isClosed()) {
                        System.out.println("Socket closed!");
                        return;
                    }
                    System.out.println("...");
                    String request = socketManager.read();
                    if ((request == null) || request.equals("quit")) {
                        socketManager.getSocket().close();
                        break;
                    }
                    handleRequest(request);
                    
                }
            } catch (Exception e) {
                System.err.println("Socket closed: " + socketManager.toString());
            } finally {
                try {
                    socket.close();
                } catch (Exception e) {
                    System.err.println("Error closing socket");
                    e.printStackTrace();
                }
            }
            System.out.println("*socket closed: " + socketManager.toString());
        }
    }

}
