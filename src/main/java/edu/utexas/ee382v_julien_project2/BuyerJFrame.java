package edu.utexas.ee382v_julien_project2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

public class BuyerJFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private DefaultTableModel attributesTableModel;
    private DefaultTableModel interestsTableModel;
    private JPanel contentPane;
    private JTable tableInterests;
    private JTextField txtName;
    private JTextField txtMinBid;
    private JTextField txtAttribute;
    private JTable tableAttributes;
    private JLabel lblStatus;
    private Client client;
    private JPanel items;
    private HashMap<Integer, ItemJPanel> itemPanelsMap;

    /**
     * Create the frame.
     */
    public BuyerJFrame(Client client) {
        
        itemPanelsMap = new HashMap<Integer, ItemJPanel>();
        this.client = client;
        setTitle("Buyer Client ID:" + client.getId());
        setBounds(100, 100, 816, 647);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        
        JPanel panelForm = new JPanel();
        
        interestsTableModel = new DefaultTableModel();
        String[] colNameInterests = {"Name", "Attributes", "Min Bid"};
        interestsTableModel.setColumnIdentifiers(colNameInterests);
        tableInterests = new JTable(interestsTableModel);
        JScrollPane scrollPaneInterests = new JScrollPane(tableInterests);
        
        JPanel panelItems = new JPanel(new BorderLayout(4, 4));
        panelItems.setBorder(new TitledBorder("Items"));
        
        GroupLayout gl_contentPane = new GroupLayout(contentPane);
        gl_contentPane.setHorizontalGroup(
            gl_contentPane.createParallelGroup(Alignment.LEADING)
                .addComponent(panelForm, GroupLayout.DEFAULT_SIZE, 811, Short.MAX_VALUE)
                .addGroup(gl_contentPane.createSequentialGroup()
                    .addComponent(scrollPaneInterests, GroupLayout.PREFERRED_SIZE, 302, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(panelItems, GroupLayout.PREFERRED_SIZE, 499, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        gl_contentPane.setVerticalGroup(
            gl_contentPane.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_contentPane.createSequentialGroup()
                    .addComponent(panelForm, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
                        .addComponent(scrollPaneInterests, GroupLayout.PREFERRED_SIZE, 474, GroupLayout.PREFERRED_SIZE)
                        .addComponent(panelItems, GroupLayout.PREFERRED_SIZE, 475, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        
        JLabel lblInterestName = new JLabel("Interest Name");
        
        JLabel lblInterestMinBid = new JLabel("Interest Min Bid");
        
        txtName = new JTextField();
        txtName.setText("name");
        txtName.setColumns(10);
        
        txtMinBid = new JTextField();
        txtMinBid.setText("min bid");
        txtMinBid.setColumns(10);
        
        JLabel lblInterestAttributes = new JLabel("Interest Attributes");
        
        txtAttribute = new JTextField();
        txtAttribute.setText("attribute");
        txtAttribute.setColumns(10);
        
        JButton btnAddAttribute = new JButton("Add");
        btnAddAttribute.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                addAttribute(txtAttribute.getText());
                txtAttribute.setText("");
            }
        });
        
        JScrollPane scrollPaneAttributes = new JScrollPane();
        
        JButton btnAddInterest = new JButton("Add Interest");
        btnAddInterest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    String name = txtName.getText();
                    int rows = attributesTableModel.getRowCount();
                    String[] attributes = new String[rows];
                    for (int index = 0; index < rows; index ++) {
                        attributes[index] = (String) attributesTableModel.getValueAt(index, 0);
                    }
                    addInterest(name, attributes, txtMinBid.getText());
                    lblStatus.setText("Interest Added");
                } catch (Exception exc) {
                    System.out.println("Invalid inputs");
                    exc.printStackTrace();
                    lblStatus.setText("Invalid inputs");
                }
            }
        });
        
        JButton btnReset = new JButton("Reset Form");
        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                resetForm();
            }
        });
        
        lblStatus = new JLabel("Status");
        
        GroupLayout gl_panelForm = new GroupLayout(panelForm);
        gl_panelForm.setHorizontalGroup(
            gl_panelForm.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_panelForm.createSequentialGroup()
                    .addGroup(gl_panelForm.createParallelGroup(Alignment.LEADING)
                        .addGroup(gl_panelForm.createSequentialGroup()
                            .addGroup(gl_panelForm.createParallelGroup(Alignment.LEADING)
                                .addGroup(gl_panelForm.createSequentialGroup()
                                    .addGap(16)
                                    .addComponent(lblInterestName))
                                .addGroup(gl_panelForm.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(lblInterestMinBid)))
                            .addPreferredGap(ComponentPlacement.UNRELATED)
                            .addGroup(gl_panelForm.createParallelGroup(Alignment.LEADING)
                                .addGroup(gl_panelForm.createSequentialGroup()
                                    .addComponent(txtName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addGap(18)
                                    .addComponent(lblInterestAttributes))
                                .addComponent(txtMinBid, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGroup(gl_panelForm.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(lblStatus, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE)))
                    .addGap(18)
                    .addGroup(gl_panelForm.createParallelGroup(Alignment.TRAILING, false)
                        .addComponent(scrollPaneAttributes, 0, 0, Short.MAX_VALUE)
                        .addComponent(txtAttribute, GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE))
                    .addGroup(gl_panelForm.createParallelGroup(Alignment.TRAILING)
                        .addGroup(gl_panelForm.createParallelGroup(Alignment.LEADING)
                            .addGroup(gl_panelForm.createSequentialGroup()
                                .addPreferredGap(ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
                                .addComponent(btnAddInterest)
                                .addContainerGap())
                            .addGroup(gl_panelForm.createSequentialGroup()
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addComponent(btnAddAttribute, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap()))
                        .addGroup(gl_panelForm.createSequentialGroup()
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addComponent(btnReset)
                            .addContainerGap())))
        );
        gl_panelForm.setVerticalGroup(
            gl_panelForm.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_panelForm.createSequentialGroup()
                    .addGroup(gl_panelForm.createParallelGroup(Alignment.BASELINE)
                        .addComponent(txtName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblInterestName)
                        .addComponent(lblInterestAttributes)
                        .addComponent(txtAttribute, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnAddAttribute, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addGroup(gl_panelForm.createParallelGroup(Alignment.TRAILING)
                        .addGroup(gl_panelForm.createSequentialGroup()
                            .addGroup(gl_panelForm.createParallelGroup(Alignment.TRAILING)
                                .addGroup(gl_panelForm.createSequentialGroup()
                                    .addGroup(gl_panelForm.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(txtMinBid, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblInterestMinBid))
                                    .addPreferredGap(ComponentPlacement.RELATED, 32, Short.MAX_VALUE))
                                .addGroup(gl_panelForm.createSequentialGroup()
                                    .addComponent(btnReset)
                                    .addPreferredGap(ComponentPlacement.RELATED)))
                            .addGroup(gl_panelForm.createParallelGroup(Alignment.TRAILING)
                                .addComponent(btnAddInterest)
                                .addComponent(lblStatus)))
                        .addComponent(scrollPaneAttributes, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap())
        );
        
        tableAttributes = new JTable();
        scrollPaneAttributes.setViewportView(tableAttributes);
        panelForm.setLayout(gl_panelForm);
        
        contentPane.setLayout(gl_contentPane);
        
        items = new JPanel(new GridLayout(0, 1, 3, 3));
        panelItems.add(new JScrollPane(items), BorderLayout.CENTER);
        resetForm();
        
        client.attachBuyerFrame(this);
//        addItem(BidItem.parseBidItem("1:woody:toy:1.0"));
    }
    
    public void addItem(BidItem bidItem) {
        ItemJPanel itemPanel = new ItemJPanel(bidItem, client);
        items.add(itemPanel);
        items.revalidate();
        items.repaint();
        itemPanelsMap.put(bidItem.getId(), itemPanel);
    }
    
    public void resetForm() {
        txtName.setText("");
        txtAttribute.setText("");
        txtMinBid.setText("");
        attributesTableModel = new DefaultTableModel();
        String[] colNameAttributes = {"Attribute"};
        attributesTableModel.setColumnIdentifiers(colNameAttributes);
        tableAttributes.setModel(attributesTableModel);
        attributesTableModel.fireTableDataChanged();
    }
    
    public void addAttribute(String attribute) {
        Object[] row = {attribute};
        attributesTableModel.addRow(row);
        tableAttributes.setModel(attributesTableModel);
        attributesTableModel.fireTableDataChanged();
    }
    
    public void addInterest(String name, String[] attributes, String minBid) {
        boolean minBidValid = true;
        if (!minBid.isEmpty()) {
            try {
                Double minBidD = Double.parseDouble(minBid);
                if (minBidD < 0) {
                    minBidValid = false;
                }
            } catch (Exception e) {
                minBidValid = false;
            }
        }
        if (!minBidValid) {
            lblStatus.setText("Min Bid must be a positive number, empty, or zero!");
        } else {
            String[] data = {name, Arrays.asList(attributes).toString(), minBid};
            interestsTableModel.addRow(data);
            tableInterests.setModel(interestsTableModel);
            interestsTableModel.fireTableDataChanged();
            resetForm();
            client.subscribeInterest(name, attributes, minBid);
        }
    }
    
    public void handleSaleFinalized(String content) {
        try {
            String[] args = content.split(":");
            Integer buyerId = Integer.parseInt(args[0]);
            Integer itemId = Integer.parseInt(args[1]);
            Double bid = Double.parseDouble(args[2]);
            ItemJPanel itemPanel = itemPanelsMap.get(itemId);
            BidItem bidItem = itemPanel.getBidItem();
            bidItem.setById(buyerId);
            bidItem.setCurrentBid(bid);
            bidItem.setIsFinal(true);
            itemPanel.setBidItem(bidItem);
            itemPanelsMap.put(itemId, itemPanel);
        } catch (Exception e) {
            System.err.println("Error handling sale finalized publication");
            e.printStackTrace();
        }
    }
    
    public void handleBidUpdate(String content) {
        try {
            String[] args = content.split(":");
            Integer buyerId = Integer.parseInt(args[0]);
            Integer itemId = Integer.parseInt(args[1]);
            Double bid = Double.parseDouble(args[2]);
            ItemJPanel itemPanel = itemPanelsMap.get(itemId);
            BidItem bidItem = itemPanel.getBidItem();
            bidItem.setById(buyerId);
            bidItem.setCurrentBid(bid);
            itemPanel.setBidItem(bidItem);
            itemPanel.autoBid();
            itemPanelsMap.put(itemId, itemPanel);
        } catch (Exception e) {
            System.err.println("Invalid bid update content:" + content);
            e.printStackTrace();
        }
    }
}
