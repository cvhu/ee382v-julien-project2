package edu.utexas.ee382v_julien_project2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

public class SellerJFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private DefaultTableModel attributeTableModel;
    private JPanel contentPane;
    private JTextField txtName;
    private JTextField txtAttributes;
    private JTextField txtMinBid;
    private JLabel lblStatus;
    private JTable tableAttributes;
    private Client client;
    private JPanel items;
    private HashMap<Integer, ItemSellJPanel> itemPanelsMap;

    /**
     * Create the frame.
     */
    public SellerJFrame(Client client) {
        if (client.isClosed()) {
            this.dispose();
        }
        this.itemPanelsMap = new HashMap<Integer, ItemSellJPanel>();
        this.client = client;
        setTitle("Seller Client ID:" + client.getId());
        setBounds(100, 100, 546, 706);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        
        JPanel addFormPanel = new JPanel();
        
        JPanel itemsPanel = new JPanel();
        
        lblStatus = new JLabel("Status: ");
        GroupLayout gl_contentPane = new GroupLayout(contentPane);
        gl_contentPane.setHorizontalGroup(
            gl_contentPane.createParallelGroup(Alignment.TRAILING)
                .addGroup(gl_contentPane.createSequentialGroup()
                    .addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
                        .addComponent(itemsPanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 539, Short.MAX_VALUE)
                        .addComponent(addFormPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblStatus, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 527, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap())
        );
        gl_contentPane.setVerticalGroup(
            gl_contentPane.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_contentPane.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(addFormPanel, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(lblStatus)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(itemsPanel, GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
                    .addContainerGap())
        );
        
        txtName = new JTextField();
        txtName.setText("Name");
        txtName.setColumns(10);
        
        txtAttributes = new JTextField();
        txtAttributes.setText("Attributes");
        txtAttributes.setColumns(10);
        
        txtMinBid = new JTextField();
        txtMinBid.setText("Min Bid");
        txtMinBid.setColumns(10);
        
        JLabel lblName = new JLabel("Name");
        
        JLabel lblAttribute = new JLabel("Attribute");
        
        JLabel lblMinBid = new JLabel("Min Bid");
        
        JButton btnCreateNewItem = new JButton("Create New Item");
        btnCreateNewItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = txtName.getText();
                    int rows = attributeTableModel.getRowCount();
                    String[] attributes = new String[rows];
                    for (int index = 0; index < rows; index ++) {
                        attributes[index] = (String) attributeTableModel.getValueAt(index, 0);
                    }
                    Double minBid = Double.parseDouble(txtMinBid.getText());
                    if (!name.isEmpty() && (attributes.length > 0) && minBid >= 0) {
                        BidItem bidItem = new BidItem(name, attributes, minBid);
                        addBidItem(bidItem);
                        lblStatus.setText("Success");
                    } else {
                        System.out.println("All inputs required");
                        lblStatus.setText("All inputs required");
                    }
                } catch (Exception exc) {
                    System.out.println("Invalid inputs");
                    exc.printStackTrace();
                    lblStatus.setText("Invalid inputs");
                }
            }
        });
        
        JButton btnAdd = new JButton("Add");
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                addAttribute(txtAttributes.getText());
                txtAttributes.setText("");
            }
        });
        
        JScrollPane scrollPaneAttributes = new JScrollPane();
        
        JButton btnResetForm = new JButton("Reset Form");
        btnResetForm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetForm();
            }
        });
        
        GroupLayout gl_addFormPanel = new GroupLayout(addFormPanel);
        gl_addFormPanel.setHorizontalGroup(
            gl_addFormPanel.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_addFormPanel.createSequentialGroup()
                    .addGroup(gl_addFormPanel.createParallelGroup(Alignment.LEADING)
                        .addGroup(gl_addFormPanel.createSequentialGroup()
                            .addGap(26)
                            .addGroup(gl_addFormPanel.createParallelGroup(Alignment.TRAILING)
                                .addComponent(lblMinBid)
                                .addComponent(lblName))
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addGroup(gl_addFormPanel.createParallelGroup(Alignment.LEADING)
                                .addGroup(gl_addFormPanel.createSequentialGroup()
                                    .addComponent(txtName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addGap(52)
                                    .addComponent(lblAttribute))
                                .addComponent(txtMinBid, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addGroup(gl_addFormPanel.createParallelGroup(Alignment.LEADING)
                                .addGroup(gl_addFormPanel.createSequentialGroup()
                                    .addComponent(txtAttributes, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnAdd))
                                .addComponent(scrollPaneAttributes, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)))
                        .addGroup(gl_addFormPanel.createSequentialGroup()
                            .addGap(112)
                            .addComponent(btnCreateNewItem)
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addComponent(btnResetForm)))
                    .addContainerGap())
        );
        gl_addFormPanel.setVerticalGroup(
            gl_addFormPanel.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_addFormPanel.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(gl_addFormPanel.createParallelGroup(Alignment.BASELINE)
                        .addComponent(lblName)
                        .addComponent(txtName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblAttribute, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtAttributes, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnAdd))
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addGroup(gl_addFormPanel.createParallelGroup(Alignment.LEADING)
                        .addGroup(gl_addFormPanel.createParallelGroup(Alignment.BASELINE)
                            .addComponent(lblMinBid)
                            .addComponent(txtMinBid, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(scrollPaneAttributes, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(gl_addFormPanel.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnCreateNewItem)
                        .addComponent(btnResetForm))
                    .addGap(45))
        );
        
        tableAttributes = new JTable();
        scrollPaneAttributes.setViewportView(tableAttributes);
        addFormPanel.setLayout(gl_addFormPanel);
        contentPane.setLayout(gl_contentPane);
        
        itemsPanel.setLayout(new BorderLayout(4, 4));
        itemsPanel.setBorder(new TitledBorder("Items for Sale"));
        
        items = new JPanel(new GridLayout(0, 1, 3, 3));
        itemsPanel.add(new JScrollPane(items), BorderLayout.CENTER);
        
        resetForm();
        
        client.attachSellerFrame(this);
//        addBidItem(BidItem.parseBidItem("1:woody:toy,disney:10.0"));
    }
    
    public void resetForm() {
        txtName.setText("");
        txtAttributes.setText("");
        txtMinBid.setText("");
        attributeTableModel = new DefaultTableModel();
        String[] colNameAttributes = {"Attribute"};
        attributeTableModel.setColumnIdentifiers(colNameAttributes);
        tableAttributes.setModel(attributeTableModel);
        attributeTableModel.fireTableDataChanged();
    }
    
    public void addAttribute(String attribute) {
        Object[] row = {attribute};
        attributeTableModel.addRow(row);
        tableAttributes.setModel(attributeTableModel);
        attributeTableModel.fireTableDataChanged();
    }
    
    
    //"Item Id", "Name", "Attribute", "Min Bid", "Current Bid", "Winner ID", "Final?"
    public void addBidItem(BidItem bidItem) {
        System.out.println(bidItem.toString());
        ItemSellJPanel itemPanel = new ItemSellJPanel(bidItem, client);
        items.add(itemPanel);
        items.revalidate();
        items.repaint();
        itemPanelsMap.put(bidItem.getId(), itemPanel);
        
        client.publishItem(bidItem);
        client.subscribeReceiveBid(bidItem.getId());
        resetForm();
    }
    
    public void handleBid(String content) {
        try {
            String[] args = content.split(":");
            Integer buyerId = Integer.parseInt(args[0]);
            Integer itemId = Integer.parseInt(args[1]);
            Double bid = Double.parseDouble(args[2]);
            ItemSellJPanel itemPanel = itemPanelsMap.get(itemId);
            BidItem bidItem = itemPanel.getBidItem();
            if (bidItem.takeBid(buyerId, bid)) {
                itemPanel.setBidItem(bidItem);
                client.publishBidUpdate(buyerId, itemId, bid);
                itemPanelsMap.put(itemId, itemPanel);
            }
        } catch (Exception e) {
            System.err.println("Invalid bid content:" + content);
            e.printStackTrace();
        }
    }
}
